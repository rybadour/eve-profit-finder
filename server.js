var Express = require('express');
var App = Express();
var https = require('https');

App.use('/', Express.static('./'));
App.get('/close-systems/:system/:distance', function (req, res) {
	https.get({
		host: 'trades.eve-price.com',
		port: 443,
		path: '/system-distance/' + [req.params.system, req.params.distance].join('/'),
	}, function (traderRes) {
		traderRes.on('data', function (chunk) {
			res.write(chunk);
		});
		traderRes.on('end', function (chunk) {
			res.end(chunk);
		});
	});
});

var port = 9001;
App.listen(port);
console.log('Listening on Port ' + port);
