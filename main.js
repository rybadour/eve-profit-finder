/**
 * Notes:
 * - Add testLevel to reprocessingSkills.json to see projected refined value
 */

var oreDefs = null;
var compressedOreDefs = null;
var refinedDefs = null;
var regionDefs = null;
var systemDefs = null;
var reprocessingSkills = null;
var characterSkills = null;
var securityClasses = null;
var defPromises = [
	$.getJSON('ore.json', function (data) {
		oreDefs = data;
	}),
	$.getJSON('compressedOre.json', function (data) {
		compressedOreDefs = data;
	}),
	$.getJSON('refined.json', function (data) {
		refinedDefs = data;
	}),
	$.getJSON('regions.json', function (data) {
		regionDefs = data;
	}),
	$.getJSON('systems.json', function (data) {
		systemDefs = data;
	}),
	$.getJSON('reprocessingSkills.json', function (data) {
		reprocessingSkills = data;
	}),
	$.getJSON('securityClasses.json', function (data) {
		securityClasses = data;
	}),
];

var REFRESH_TIME = 24 * 60 * 60 * 1000; // 1 day /ms

function getThing(defs, name) {
	var thing = defs.filter(function (def) {
		return def.name === name;
	});
	return (thing && thing.length > 0 ? thing[0] : null);
}

function getThingById(defs, id) {
	var thing = defs.filter(function (def) {
		return def.typeId === id;
	});
	return (thing && thing.length > 0 ? thing[0] : null);
}

function createUrl(path, params) {
	var paramList = [];
	var paramStr = '?' + Object.keys(params).map(function (key) {
		return key + '=' + params[key];
	}).join('&');
	return path + paramStr;
}

function _getXmlNodeAttribute(node, attr) {
	for (var a = 0; a < node.attributes.length; ++a) {
		var attribute = node.attributes[a];
		if (attribute.nodeName === attr) {
			return attribute.nodeValue;
		}
	}
	return null;
}

function isOreInSecurityClass(securityClass, oreDef) {
	return securityClasses[securityClass].some(function (oreName) {
		return oreDef.name.match(oreName);
	});
}

function systemToString(system) {
	return system.name + '(' + system.distance + ')';
}

var savedResults = {};
var typeIdsToQuery = [];
var buyDataBySystem = {};
var characters = null;
var preferences = {
	workingSystem:    "Amarr",
	distance:         3,
	maxRawDistance:   0,
	shownValue:       "avg",
	minShownValue:    80,
	oreLimit:         "Vitreous Mercoxit",
	stationYield:     50,
	setSkillsToMax:   false,
	apiKey:           '',
	verificationCode: '',
	characterId:      '',
};

function getBuyValue(systemPrices, systemId, itemId, shownValue) {
	var buyData = systemPrices[systemId].items[itemId];
	switch (shownValue) {
		case "average": return buyData.avg;
		case "max": return buyData.max;
		case "min": return buyData.min;
	}
}

function getRefineRatio(oreDef, stationYield, characterSkills) {
	var refineYield = stationYield / 100;
	reprocessingSkills.forEach(function (reprocessing) {
		if (reprocessing.oreAffected === true || oreDef.name.match(reprocessing.oreAffected)) {
			var skillLevel = getSkillLevel(characterSkills, reprocessing.skillId);
			var level = (reprocessing.testLevel ? reprocessing.testLevel : skillLevel);
			refineYield *= 1 + (reprocessing.effectPerLevel * level);
		}
	});
	return refineYield;
}

function forEachOre(oreDefs, systemSecurityClass, isCompressed, oreLimit, iterateFunc) {
	var hitLimit = false;
	oreDefs.forEach(function (oreDef, o) {
		if (systemSecurityClass && !isOreInSecurityClass(systemSecurityClass, oreDef)) return;
		if (hitLimit) return;
		if (isCompressed && oreDef.uncompressedTypeId === undefined) return;
		if (oreDef.name === oreLimit) {
			hitLimit = true;
		}

		iterateFunc(oreDef, o);
	});
}

function querySystemSecurityClass(systemName) {
	var systemId = getThing(systemDefs, systemName).typeId;
	var url = 'https://crest-tq.eveonline.com/solarsystems/' + systemId + '/';
	return $.getJSON(url).then(function (system) {
		return system.securityClass;
	});
}

var MAX_SKILLS = 'ASSUMED_MAX_SKILLS';
function queryCharacterSkills(apiKey, verificationCode, characterId, setSkillsToMax) {
	if (setSkillsToMax) {
		// Returns a constant that represents that all skills should be considered level 5.
		// This is necessary because the list returned only includes learnt skills.
		return MAX_SKILLS;
	}

	if (apiKey && verificationCode) {
		if (characterId) {
			var params = {
				characterID: characterId,
				keyID: apiKey,
				vCode: verificationCode,
			};
			return $.get(createUrl('https://api.eveonline.com/char/Skills.xml.aspx', params)).then(function (data) {
				characterSkills = {};
				var results = data.childNodes[0].childNodes[3].childNodes[3];
				results.childNodes.forEach(function (child) {
					if (child.nodeType !== 1) return;
					var skill = {};
					for (var a = 0; a < child.attributes.length; ++a) {
						var attribute = child.attributes[a];
						skill[attribute.nodeName] = attribute.nodeValue;
					}
					characterSkills[skill.typeID] = skill;
				});
				return characterSkills;
			});
		} else {
			var params = {
				keyID: apiKey,
				vCode: verificationCode,
			};
			return $.get(createUrl('https://api.eveonline.com/account/Characters.xml.aspx', params)).then(function (data) {
				var charactersNode = data.childNodes[0].childNodes[3].childNodes[1];
				characters = [];
				charactersNode.childNodes.forEach(function (node) {
					if (node.nodeType !== 1) return;
					characters.push({
						name: _getXmlNodeAttribute(node, 'name'),
						characterId: _getXmlNodeAttribute(node, 'characterID'),
					});
				});
				characterId = characters[0].characterId;
				populateCharacterSelect();

				return queryCharacterSkills(apiKey, verificationCode, characterId, setSkillsToMax);
			});
		}
	} else {
		return false;
	}
}

function getSkillLevel(skills, skillId) {
	if (skills === MAX_SKILLS) {
		return 5;
	} else {
		var skillLevel = skills[skillId];
		return (skillLevel ? parseInt(skillLevel.level, 10) : 0)
	}
}

function querySystemsPrices(systems) {
	var promises = systems.map(function (system) {
		var id = system.typeId;

		if (buyDataBySystem[id] && (buyDataBySystem[id].timestamp + REFRESH_TIME > Date.now())) {
			return buyDataBySystem[id];
		}

		var params =  {
			usesystem: id,
			typeid: typeIdsToQuery.join(',')
		};
		return $.get({
			url: createUrl('http://api.eve-central.com/api/marketstat/json', params),
			dataType: 'json',
		}).then(function (data) {
			var i = 0;
			var buyData = buyDataBySystem[id] = {
				systemId: id,
				items: {},
				timestamp: Date.now()
			};
			typeIdsToQuery.forEach(function (typeId) {
				buyData.items[typeId] = {
					min: data[i].buy.min,
					max: data[i].buy.max,
					avg: data[i].buy.avg,
				};
				++i;
			});
			return buyData;
		});
	});
	return Promise.all(promises).then(function (results) {
		refreshCache();
		var allPrices = {};
		results.forEach(function (systemPrices) {
			allPrices[systemPrices.systemId] = systemPrices;
		});
		return allPrices;
	});
}

function loadFromCache() {
	var storageJson = localStorage.getItem("cache");
	if (storageJson) {
		var cache = JSON.parse(storageJson);
		buyDataBySystem = cache.buyDataBySystem;
		$.extend(preferences, cache.preferences);
		characters = (cache.characters ? cache.characters : null);
	}
}

function refreshCache() {
	localStorage.setItem("cache", JSON.stringify({
		buyDataBySystem: buyDataBySystem,
		preferences: preferences,
		characters: characters,
	}));
}

function populateWorkingSystemSelect() {
	var $select = $('#workingSystem');
	systemDefs.map(function (system) {
		return system.name;
	}).filter(function (name) {
		return name.match(/^[a-zA-z ]+$/);
	}).sort().forEach(function (name) {
		$select.append('<option value="' + name + '">' + name + '</option>');
	});
}

function populateOreLimitSelect() {
	var $select = $('#oreLimit');
	oreDefs.forEach(function (ore) {
		$select.append('<option value="' + ore.name + '">' + ore.name + '</option>');
	});
}

function populateCharacterSelect() {
	var $select = $('#characterId');
	if (characters) {
		$select.prop('disabled', false);
		characters.forEach(function (character) {
			$select.append('<option value="' + character.characterId + '">' + character.name + '</option>');
		});
	} else {
		$select.prop('disabled', true);
	}
}

function preferenceChanged(pref) {
		refreshResults('pref:' + pref);
		refreshOreMarket();
}

function bindPreferences() {
	Object.keys(preferences).forEach(function (key) {
		var $control = $('#' + key);
		// A function that accepts the value or nothing and then sets or gets respectively.
		var valueFunc = ($control.attr('type') === 'checkbox' ? $control.prop.bind($control, 'checked') : $control.val.bind($control));
		valueFunc(preferences[key]);
		$control.change(function (key, valueFunc) {
			preferences[key] = valueFunc();
			preferenceChanged(key);
		}.bind($control, key, valueFunc));
	});

	$('#filters label i').each(function () {
		var icon = $(this);
		if (icon.data('help-id')) {
			icon.popover({
				html: true,
				content: $('#help-id-' + icon.data('help-id')).html(),
				placement: 'bottom',
			});
		}
	});
}

function querySystemsWithinDistance(system, distance) {
	return $.get({
		url: "https://trades.eve-price.com/system-distance/" + [system, distance].join('/'),
		dataType: "JSON",
	}).then(function (response) {
		return response.results.map(function (system) {
			var systemDef = getThing(systemDefs, system.solarSystemName);
			return {
				typeId: systemDef.typeId,
				name: systemDef.name,
				distance: system.distance,
			};
		});
	});
}

function getAllSystemsInSameConstellation(system) {
	var constellationId = getThing(systemDefs, system).constellationId;
	return systemDefs.filter(function (def) {
		return def.constellationId === constellationId;
	});
}

/**
 * Definition of all result sets that are cached in memory and their dependencies.
 */
var standardOreDeps = ['systemsWithinDistance', 'systemsPrices', 'workingSystemSecurityClass', 'pref:oreLimit', 'pref:shownValue'];
var resultsDeps = {
	allPrices: {
		queryFunc: collectAndTransformPrices,
		deps: ['rawOrePrices', 'compressedOrePrices', 'refinedOrePrices', 'pref:minShownValue'],
	},
	rawOrePrices: {
		queryFunc: getRawOrePrices,
		deps: standardOreDeps.concat(['pref:maxRawDistance']),
	},
	compressedOrePrices: {
		queryFunc: getCompressedOrePrices,
		deps: standardOreDeps.concat(['pref:maxRawDistance']),
	},
	refinedOrePrices: {
		queryFunc: getRefinedOrePrices,
		deps: standardOreDeps.concat(['characterSkills', 'pref:stationYield']),
	},
	systemsWithinDistance: {
		queryFunc: querySystemsWithinDistance,
		deps: ['pref:workingSystem', 'pref:distance'],
	},
	systemsPrices: {
		queryFunc: querySystemsPrices,
		deps: ['systemsWithinDistance'],
	},
	workingSystemSecurityClass: {
		queryFunc: querySystemSecurityClass,
		deps: ['pref:workingSystem'],
	},
	characterSkills: {
		queryFunc: queryCharacterSkills,
		deps: ['pref:apiKey', 'pref:verificationCode', 'pref:characterId', 'pref:setSkillsToMax'],
	},
};

function gatherResults(resultType) {
	var result = resultsDeps[resultType];
	var depPromises = result.deps.map(function (depType) {
		var matches = depType.match(/^pref:(.*)$/);
		if (matches && matches.length > 0) {
			return preferences[matches[1]];
		} else {
			return gatherResults(depType);
		}
	});
	return Promise.all(depPromises).then(function (realizedDeps) {
		if (savedResults[resultType] === null || savedResults[resultType] === undefined) {
			savedResults[resultType] = result.queryFunc.apply(null, realizedDeps);
		}
		return savedResults[resultType];
	});
}

function refreshResults(depType) {
	Object.keys(resultsDeps).forEach(function (resultType) {
		var result = resultsDeps[resultType];
		if (result.deps.indexOf(depType) > -1) {
			savedResults[resultType] = null;
			// Refresh results that depend on this as well
			refreshResults(resultType);
		}
	});
}

function refreshOreMarket() {
	renderTable(gatherResults('allPrices'));
}

function getRawOrePrices(systems, systemPrices, workingSystemSecurityClass, oreLimit, shownValue, maxRawDistance) {
	var prices = [];
	systems.forEach(function (system) {
		if (system.distance <= parseInt(maxRawDistance, 10)) {
			forEachOre(oreDefs, workingSystemSecurityClass, false, oreLimit, function (oreDef, o) {
				var value = getBuyValue(systemPrices, system.typeId, oreDef.typeId, shownValue);
				value /= oreDef.volume;
				if (value > 0) {
					prices.push({
						itemName: oreDef.name,
						systems: [system],
						price: value,
					});
				}
			});
		}
	});
	return prices;
}

function getCompressedOrePrices(systems, systemPrices, workingSystemSecurityClass, oreLimit, shownValue) {
	var prices = [];
	systems.forEach(function (system) {
		// Compressed Ore
		forEachOre(compressedOreDefs, workingSystemSecurityClass, true, oreLimit, function (compOreDef, o) {
			var oreDef = getThingById(oreDefs, compOreDef.uncompressedTypeId);
			var value = getBuyValue(systemPrices, system.typeId, compOreDef.typeId, shownValue);
			value /= 100;
			value /= oreDef.volume;
			if (value > 0) {
				prices.push({
					itemName: compOreDef.name,
					systems: [system],
					price: value,
				});
			}
		});
	});
	return prices;
}

function getRefinedOrePrices(systems, systemPrices, workingSystemSecurityClass, oreLimit, shownValue, skills, stationYield) {
	var prices = [];
	if (skills) {
		var relevantRefined = [];
		forEachOre(oreDefs, workingSystemSecurityClass, false, oreLimit, function (oreDef, o) {
			refinedDefs.forEach(function (refined) {
				var key = refined.name.toLowerCase();
				if (oreDef[key] !== null && oreDef[key] > 0 && relevantRefined.indexOf(refined) === -1) {
					relevantRefined.push(refined);
				}
			});
		});

		var bestRefinedPrices = relevantRefined.map(function (refinedDef) {
			var highest = 0;
			var highestSystem = null;
			systems.forEach(function (system) {
				var price = getBuyValue(systemPrices, system.typeId, refinedDef.typeId, shownValue);
				if (highest < price) {
					highest = price;
					highestSystem = system;
				}
			});
			return {
				refinedDef: refinedDef,
				price: highest,
				system: highestSystem,
			};
		}).filter(function (best) {
			return best.system !== null;
		});

		forEachOre(oreDefs, workingSystemSecurityClass, false, oreLimit, function (oreDef, o) {
			var relevantSystems = []; 
			var totalPrice = bestRefinedPrices.filter(function (best) {
				var key = best.refinedDef.name.toLowerCase();
				return (oreDef[key] !== null && oreDef[key] > 0);
			}).map(function (best) {
				var key = best.refinedDef.name.toLowerCase();

				relevantSystems.push(best.system);

				// Note: this may be confusing to people but the reprocessing changed in 2014. Now all
				// ore is processed in batches of 100.
				var oreProduce = oreDef[key] / 100;
				var refineRatio = getRefineRatio(oreDef, stationYield, skills);
				return (oreProduce / oreDef.volume) * best.price * refineRatio;
			}).reduce(function (a, b) {
				return a + b;
			});

			prices.push({
				itemName: oreDef.name + ' (Refined)',
				systems: relevantSystems,
				price: totalPrice,
			});
		});
	}
	return prices;
}

function collectAndTransformPrices(orePrices, compressedPrices, refinedPrices, minShownValue) {
	return [].concat(orePrices, compressedPrices, refinedPrices).filter(function (price) {
		return price.price >= minShownValue;
	}).sort(function (a, b) {
		// Sort prices highest to lowest
		if (a.price < b.price) {
			return 1;
		} else if (a.price > b.price) {
			return -1;
		} else {
			return 0;
		}
	}).map(function (rawPrice) {
		return {
			itemName: rawPrice.itemName,
			system:   rawPrice.systems.map(systemToString).join(', '),
			price:    rawPrice.price.toLocaleString('en-US', {maximumFractionDigits: 2}) 
		};
	});
}

function renderTable(prices) {
	prices.then(function (prices) {
		var $table = $('#oreMarket').empty();
		$table.stickyTableHeaders('destroy');

		var $headers = $('<tr>');
		$headers.append($('<th>Item</th>'));
		$headers.append($('<th>System</th>'));
		$headers.append($('<th>Price</th>'));
		$table.prepend($('<thead>').append($headers));

		var $body = $('<tbody>');
		prices.forEach(function (price) {
			var $row = $('<tr>');
			$row.append('<td>' + price.itemName + '</td>');
			$row.append('<td>' + price.system + '</td>');
			$row.append('<td>' + price.price + '</td>');
			$body.append($row);
		});
		$table.append($body);

		$table.stickyTableHeaders({
			fixedOffset: 20,
			scrollableArea: $('#content')
		});
		$(window).scroll(function () {
			$('#oreMarket').stickyTableHeaders();
		});

		refreshCache();
	});
}

$(window).ready(function () {
	loadFromCache();

	Promise.all(defPromises).then(function () {
		typeIdsToQuery = oreDefs.map(function (ore) {
			return ore.typeId;
		}).concat(
			refinedDefs.map(function (refined) {
				return refined.typeId;
			})
		).concat(
			compressedOreDefs.map(function (comp) {
				return comp.typeId;
			})
		);

		populateWorkingSystemSelect();
		populateOreLimitSelect();
		populateCharacterSelect();
		bindPreferences();
		refreshOreMarket();
	});
});
